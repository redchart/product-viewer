import 'package:flutter/material.dart';

import 'package:go_router/go_router.dart';
import 'package:product_viewer/screens/products_details_screen.dart';
import 'package:product_viewer/screens/products_screen.dart';

final GoRouter router = GoRouter(
  initialLocation: '/',
  routes: <RouteBase>[
    GoRoute(
      path: '/',
      builder: (BuildContext context, GoRouterState state) {
        return const ProductsScreen();
      },
      routes: <RouteBase>[
        GoRoute(
          path: 'products/:id',
          builder: (BuildContext context, GoRouterState state) {
            int id = int.parse(state.pathParameters['id']!);
            return ProductDetailsScreen(id: id);
          },
        ),
      ],
    ),
  ],
);
