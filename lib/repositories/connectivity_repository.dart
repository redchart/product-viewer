import 'dart:async';
import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';

/*
* This class is used to check if the device is connected to the internet
* and to check if the device is really connected to the internet
*/

class ConnectivityRepository {
  final Connectivity _connectivity = Connectivity();
  final StreamController<bool> _controller = StreamController<bool>();

  ConnectivityRepository() {
    _connectivity.onConnectivityChanged
        .listen((List<ConnectivityResult> result) async {
      _controller.add(await _isConnected(result));
    });
  }

  Stream<bool> get connectivityStream => _controller.stream;

  Future<bool> isConnected() async {
    var result = await _connectivity.checkConnectivity();
    return _isConnected(result);
  }

  Future<bool> _isConnected(List<ConnectivityResult> connectivityResult) async {
    if (!connectivityResult.contains(ConnectivityResult.mobile) &&
        !connectivityResult.contains(ConnectivityResult.wifi)) {
      return false;
    }
    return await isReallyConnected();
  }

  void dispose() {
    _controller.close();
  }

  /*
  * This method is used to check if the device is really connected to the internet
  * by checking if it can resolve the google.com domain.
  * it can be usefull when device is connected to a wifi network but the network is not connected to the internet
  */
  Future<bool> isReallyConnected() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (e) {
      return false;
    }
    return false;
  }
}
