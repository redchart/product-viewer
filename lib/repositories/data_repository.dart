import 'package:product_viewer/data_providers/products_provider.dart';
import 'package:product_viewer/models/dtos/product_dto.dart';


class ProductsRepository {
  final ProductsProvider dataProvider = ProductsProvider();

  ProductsRepository();

  Future<List<ProductDto>> fetchData() async {
    return await dataProvider.fetchProducts();
  }

  Future<List<ProductDto>> fetchCachedData() async {
    return await dataProvider.getCachedProducts();
  }
  
}
