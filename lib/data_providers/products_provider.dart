import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:product_viewer/exceptions/exceptions.dart';
import 'package:product_viewer/models/dtos/product_dto.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProductsProvider {
  static const _keyProducts = 'products';
  static const _apiUrl = 'https://fakestoreapi.com/products';

  Future<void> _cacheProducts(List<ProductDto> products) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(
        _keyProducts, json.encode(products.map((e) => e.toJson()).toList()));
  }

  Future<List<ProductDto>> getCachedProducts() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? cachedData = prefs.getString(_keyProducts);
    if (cachedData != null) {
      List<dynamic> data = json.decode(cachedData);
      return data.map((json) => ProductDto.fromJson(json)).toList();
    }
    return [];
  }

  Future<List<ProductDto>> fetchProducts() async {
    try {
      // Fetch products from API
      final response = await http.get(Uri.parse(_apiUrl));

      if (response.statusCode == 200) {
        List<dynamic> data = json.decode(response.body);
        List<ProductDto> products =
            data.map((json) => ProductDto.fromJson(json)).toList();

        // Cache products locally
        await _cacheProducts(products);

        return products;
      } else {
        throw EndpointException('Failed to load products(server problems)');
      }
    } on EndpointException catch (_) {
      rethrow;
    } catch (e) {
      // If there is an error return cached products
      List<ProductDto> cachedProducts = await getCachedProducts();
      if (cachedProducts.isNotEmpty) {
        return cachedProducts;
      } else {
        throw CacheException('No internet and no cached data available');
      }
    }
  }
}
