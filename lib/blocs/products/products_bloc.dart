import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:product_viewer/blocs/products/products_event.dart';
import 'package:product_viewer/blocs/products/products_state.dart';
import 'package:product_viewer/repositories/connectivity_repository.dart';
import 'package:product_viewer/repositories/data_repository.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  final ProductsRepository repository;
  final ConnectivityRepository connectivityRepository;
  late final StreamSubscription connectivitySubscription;
  late bool needToSync = false;
  late bool isConnected;

  ProductsBloc(
    this.repository,
    this.connectivityRepository,
  ) : super(const ProductsState()) {
    on<ProductsLoad>(
      _onProductsLoad,
    );

    connectivityRepository.isConnected().then((value) {
      isConnected = value;
    });

    connectivitySubscription =
        connectivityRepository.connectivityStream.listen((connected) async {
      if (isConnected == false && connected == true) {
        isConnected = connected;
        if (needToSync) {
          await repository.fetchData();
          needToSync = false;
        }
      }
      isConnected = connected;
    });
  }

  Future<bool> _isConnected() async {
    bool isConnected = await connectivityRepository.isConnected();
    return isConnected;
  }

  void _onProductsLoad(ProductsLoad event, Emitter<ProductsState> emit) async {
    try {
      bool isConnected = await _isConnected();
      //here check if there is cached data
      if (isConnected == false) {
        final cachedProducts = await repository.fetchCachedData();
        if (cachedProducts.isEmpty) {
          return emit(state.copyWith(
              status: ProductsStatus.failure,
              error:
                  "No internet connection, please connect to the internet and try again."));
        }
        emit(state.copyWith(
            status: ProductsStatus.success, products: cachedProducts));
      }
      final products = await repository.fetchData();
      emit(state.copyWith(status: ProductsStatus.success, products: products));
    } catch (e) {
      emit(state.copyWith(status: ProductsStatus.failure, error: e.toString()));
    }
  }
}
