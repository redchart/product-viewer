import 'package:equatable/equatable.dart';
import 'package:product_viewer/models/dtos/product_dto.dart';

enum ProductsStatus { initial, success, failure}

class ProductsState extends Equatable {
  final List<ProductDto> products;
  final ProductsStatus status;
  final String error;

  const ProductsState({
    this.products = const <ProductDto>[],
    this.status = ProductsStatus.initial,
    this.error = '',
  });

  ProductsState copyWith({
    List<ProductDto>? products,
    ProductsStatus? status,
    String? error,
  }) {
    return ProductsState(
      products: products ?? this.products,
      status: status ?? this.status,
      error: error ?? this.error,
    );
  }

  @override
  String toString() {
    return 'ProductsState { products: ${products.length}, status: $status, error: $error }';
  }
  
  @override
  List<Object?> get props => [products, status, error];
}