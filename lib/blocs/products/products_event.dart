import 'package:equatable/equatable.dart';

sealed class ProductsEvent  extends Equatable {
  @override
  List<Object> get props => [];
}

class ProductsLoad extends ProductsEvent {}

