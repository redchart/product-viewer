import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:product_viewer/blocs/products/products_bloc.dart';
import 'package:product_viewer/blocs/products/products_state.dart';
import 'package:product_viewer/config/colors.dart';
import 'package:product_viewer/models/dtos/product_dto.dart';
import 'package:product_viewer/typography/body_small.dart';
import 'package:product_viewer/typography/headline_large.dart';
import 'package:product_viewer/widgets/rating.dart';

class ProductDetailsScreen extends StatefulWidget {
  const ProductDetailsScreen({
    super.key,
    required this.id,
  });

  final int id;

  @override
  State<ProductDetailsScreen> createState() => _ProductDetailsScreenState();
}

class _ProductDetailsScreenState extends State<ProductDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mainBgColor,
        elevation: 0,
        surfaceTintColor: mainBgColor,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: white,
          ),
          onPressed: () {
            context.pop();
          },
        ),
      ),
      extendBodyBehindAppBar: true,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: ProductDetailsView(id: widget.id),
      ),
    );
  }
}

class ProductDetailsView extends StatefulWidget {
  const ProductDetailsView({
    super.key,
    required this.id,
  });
  final int id;
  @override
  State<ProductDetailsView> createState() => _ProductDetailsViewState();
}

class _ProductDetailsViewState extends State<ProductDetailsView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        BlocBuilder<ProductsBloc, ProductsState>(
          builder: (context, state) {
            if (state.status == ProductsStatus.initial) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (state.status == ProductsStatus.failure) {
              return Center(
                child: Text(
                  'Error: ${state.error}',
                ),
              );
            } else if (state.status == ProductsStatus.success) {
              ProductDto? product = state.products.firstWhereOrNull(
                (product) => product.id == widget.id,
              );

              if (product == null) {
                return const Center(child: Text('Product not found'));
              }
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(
                        height: kToolbarHeight + 20), // Space for AppBar
                    Wrap(
                      spacing: 8.0,
                      children: [
                        HeadlineLarge(
                          color: white,
                          text: product.title,
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Ratinng(
                      rating: product.rating.rate,
                    ),
                    const SizedBox(height: 10),
                    product.imageUrl != null
                        
                        ? CachedNetworkImage(
                            height: 300,
                            imageUrl: product.imageUrl!,
                            imageBuilder: (context, imageProvider) => Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.cover,
                                  colorFilter: const ColorFilter.mode(
                                    Colors.red,
                                    BlendMode.colorBurn,
                                  ),
                                ),
                              ),
                            ),
                            placeholder: (context, url) =>
                                const CircularProgressIndicator(),
                            errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                          )
                        : Image.asset(
                            'assets/images/loading_placeholder.png', // Your bottle image
                            height: 300,
                          ),
                    const SizedBox(height: 20),
                    _renderProductDetail(product),
                    const SizedBox(height: 20),
                  ],
                ),
              );
            }
            return Container(); // Return an empty container if no valid state
          },
        ),
      ],
    );
  }

  Widget _renderProductDetail(ProductDto product) {
    return Container(
      // padding: const EdgeInsets.all(16),
      // margin: const EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        color: secondaryBgColor,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            // spacing: 8.0,
            children: [
              HeadlineLarge(
                color: white,
                text: '${product.price} \$',
              ),
            ],
          ),
          BodySmall(
            text: product.description,
            color: disabledWhite,
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}
