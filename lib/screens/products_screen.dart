import 'package:flutter/material.dart';
import 'package:product_viewer/typography/headline.dart';
import 'package:product_viewer/widgets/products_list.dart';

class ProductsScreen extends StatefulWidget {
  const ProductsScreen({super.key});

  @override
  State<ProductsScreen> createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(0, 29, 153, 225),
      appBar: PreferredSize(
        preferredSize:
            const Size.fromHeight(80.0),
        child: Container(
          padding:
              const EdgeInsets.only(top: 24.0),
          child: AppBar(
            surfaceTintColor: const Color.fromARGB(0, 29, 153, 225),
            title: const Headline(
              text: 'Products',
              color: Colors.white,
            ),
            automaticallyImplyLeading: false,
            backgroundColor: const Color.fromARGB(0, 29, 153, 225),
          ),
        ),
      ),
      body: const Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: ProductsList(),
      ),
    );
  }
}
