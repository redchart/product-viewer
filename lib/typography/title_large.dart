import 'package:flutter/material.dart';

class TitleLarge extends StatelessWidget {
  const TitleLarge({
    super.key,
    required this.color,
    required this.text,
  });

  final Color color;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: color,
        fontSize: 22.0,
        fontWeight: FontWeight.w500,
        fontFamily: 'lato',
      ),
    );
  }
}
