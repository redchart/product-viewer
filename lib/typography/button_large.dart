import 'package:flutter/material.dart';

class ButtonLarge extends StatelessWidget {
  const ButtonLarge({
    super.key,
    required this.color,
    required this.text,
  });

  final Color color;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: color,
        fontSize: 16.0,
        fontWeight: FontWeight.w600,
        fontFamily: 'lato',
      ),
    );
  }
}
