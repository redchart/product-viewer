import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:product_viewer/blocs/products/products_bloc.dart';
import 'package:product_viewer/blocs/products/products_event.dart';
import 'package:product_viewer/config/router.dart';
import 'package:product_viewer/repositories/connectivity_repository.dart';
import 'package:product_viewer/repositories/data_repository.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              ProductsBloc(ProductsRepository(), ConnectivityRepository())
                ..add(ProductsLoad()),
        ),
      ],
      child: MaterialApp.router(
        title: 'Product Viewer',
        theme: _buildTheme(Brightness.dark),
        routerConfig: router,
      ),
    );
  }

  ThemeData _buildTheme(brightness) {
    var baseTheme = ThemeData(brightness: brightness);

    return baseTheme;
  }
}
