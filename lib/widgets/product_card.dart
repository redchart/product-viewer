import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:product_viewer/models/dtos/product_dto.dart';
import 'package:product_viewer/typography/body_small.dart';
import 'package:product_viewer/typography/title_large.dart';
import 'package:product_viewer/widgets/rating.dart';

class ProductCard extends StatelessWidget {
  const ProductCard({super.key, required this.product});

  final ProductDto product;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: const BeveledRectangleBorder(),
      color: const Color(0xFF1E1E1E),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
            child: TitleLarge(
              text: product.title,
              color: Colors.white,
            ),
          ),
          Ratinng(
            rating: product.rating.rate,
          ),
          SizedBox(
            height: 200,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: product.imageUrl != null ?  CachedNetworkImage(
                  imageUrl: product.imageUrl!,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.contain,
                        colorFilter: const ColorFilter.mode(
                          Colors.red,
                          BlendMode.colorBurn,
                        ),
                      ),
                    ),
                  ),
                  placeholder: (context, url) =>
                      const CircularProgressIndicator(),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ): 
                Image.asset(
                  'assets/images/loading_placeholder.png', // Your bottle image
                  height: 300,
                ),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
            child: TitleLarge(
              text: '${product.price} \$',
              color: Colors.white,
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
            child: BodySmall(
              text: product.description,
              color: const Color(0xFFD7D5D1),
            ),
          ),
        ],
      ),
    );
  }
}
