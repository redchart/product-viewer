import 'package:flutter/material.dart';

class Ratinng extends StatelessWidget {
  const Ratinng({super.key, required this.rating});

  final int starCount = 5;
  final Color color = Colors.amber;
  final double starSize = 24;
  final double rating;

  @override
  Widget build(BuildContext context) {
    List<Widget> stars = [];
    // Loop to create star widgets
    for (int i = 1; i <= starCount; i++) {
      if (i <= rating.floor()) {
        // Filled star
        stars.add(Icon(Icons.star, color: color, size: starSize));
      } else if (i - rating < 1 && i - rating > 0) {
        // Half star
        stars.add(Icon(Icons.star_half, color: color, size: starSize));
      } else {
        // Empty star
        stars.add(Icon(Icons.star_border, color: color, size: starSize));
      }
    }

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: stars,
    );
  }
}
