import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:product_viewer/blocs/products/products_bloc.dart';
import 'package:product_viewer/blocs/products/products_event.dart';
import 'package:product_viewer/blocs/products/products_state.dart';
import 'package:product_viewer/widgets/product_card.dart';

class ProductsList extends StatefulWidget {
  const ProductsList({super.key});

  @override
  State<ProductsList> createState() => _ProductsListState();
}

class _ProductsListState extends State<ProductsList> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductsBloc, ProductsState>(
      builder: (context, state) {
        switch (state.status) {
          case ProductsStatus.failure:
            return Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(state.error),
                ElevatedButton(
                  onPressed: () {
                    context.read<ProductsBloc>().add(ProductsLoad());
                  },
                  child: const Text('Retry'),
                ),
              ],
            ));
          case ProductsStatus.success:
            if (state.products.isEmpty) {
              return const Center(
                  child: Text('No products found. Please try again later.'));
            }
            return ListView.builder(
              itemCount: state.products.length,
              itemBuilder: (context, index) {
                final product = state.products[index];
                return GestureDetector(
                  onTap: () {
                    context.push('/products/${product.id}');
                  },
                  child: ProductCard(
                    product: product,
                  ),
                );
              },
            );

          case ProductsStatus.initial:
            return const Center(
              child: CircularProgressIndicator(),
            );
        }
      },
    );
  }
}
