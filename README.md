# Product Viewer App

This project is a Flutter application designed to fetch and display a list of products from an API (Fake Store API). The app features a local caching mechanism that allows offline viewing and handles various error states related to network connectivity and API issues.

## Features

- **Product List Display**: Products are fetched from an external API and displayed in a list with images, titles, descriptions, and star ratings.
- **Product Details**: Users can tap on a product to view more detailed information on a separate screen.
- **Caching**: The app caches product data locally using `SharedPreferences`, ensuring the product list can still be viewed even if there is no internet connection.
- **Error Handling**: The app can distinguish between different error states such as network connectivity issues and API problems. Custom error messages are displayed based on the type of issue encountered.
- **Custom Widgets**: The app includes several custom widgets such as `Rating` for star ratings, `ProductCard` for individual product display, and `CustomTextStyles` for consistent styling across the app.

## Project Structure
```lib/
├── blocs/                 # Contains BLoC files for state management
├── config/                # Configuration files (e.g., colors, constants) and Routing logic using GoRouter
├── exceptions/            # Custom exception handling logic
├── models/                # Data models (e.g., DTOs)
├── repositories/          # Data handling and connectivity logic
├── screens/               # Screens for different parts of the app
├── typography/            # Reusable text styles
├── widgets/               # Reusable widgets across the app
```



## Setup Instructions

### Prerequisites

- [Flutter](https://flutter.dev/docs/get-started/install) installed on your system
- Flutter version 3.22.2
- An editor such as [VSCode](https://code.visualstudio.com/) or [Android Studio](https://developer.android.com/studio)

### Instructions for Building and Running the Project
1. Clone repository
2. flutter pub get
3. flutter run 

### Caching Mechanism

The app stores the products fetched from the API in the local storage using `SharedPreferences`. When there is no internet connection, the app attempts to load products from the cache. If the cache is unavailable, the app will notify the user of the error.

### Error Handling

The project includes custom exceptions to differentiate between different types of failures:

- **NetworkException**: Thrown when there is no internet connection.
- **EndpointException**: Thrown when the API responds with an error or fails to respond.
- **CacheException**: Thrown when there is no internet and cached data is either invalid or unavailable.

### Screens

- **ProductsScreen**: This is the main screen of the app that displays a list of products. It uses a custom `AppBar` with a title and the `ProductsList` widget to show a list of products fetched from the API. The screen is wrapped in a `Scaffold` widget to handle layout and styling. The products are fetched using the BLoC architecture and are displayed using the `ProductCard` widget. If the products are loading, a loading indicator is shown, and if there is an error, an error message with a retry option is presented.
  
- **ProductDetailsScreen**: This screen displays detailed information about a selected product. It uses a `BlocBuilder` to get the product's details from the state. The product details include the product's image, title, price, description, and rating. The screen also includes a custom `AppBar` with a back button to navigate back to the `ProductsScreen`. If there is an issue fetching the product's details, appropriate error messages or loading indicators are shown.

### Navigation

The app uses `GoRouter` for navigation between the `ProductsScreen` and `ProductDetailsScreen`. 

- **Initial Route**: The initial route is set to `/`, which loads the `ProductsScreen` to display a list of products.
  
- **Product Details Route**: When a user taps on a product, they are navigated to the `ProductDetailsScreen` using a route in the format `/products/:id`, where `id` is the ID of the selected product. The router passes the product ID to the `ProductDetailsScreen` for fetching and displaying the relevant details.
  
  
### Custom Widgets

- **Rating Widget**: A star rating widget that displays product ratings in a visual format.
- **ProductCard**: A card widget to display product information such as image, title, description, and price.
- **ProductsList**: A stateful widget that displays a list of products fetched from an API using the BLoC pattern. 
  - This widget uses a `BlocBuilder` to manage different states (`ProductsStatus.initial`, `ProductsStatus.success`, and `ProductsStatus.failure`). 
  - Depending on the state, the widget will either show a loading indicator, a list of products, or an error message with a retry button.
  - When products are successfully fetched, the list of products is displayed using a `ListView.builder`, and each item in the list is wrapped in a `ProductCard` widget.
  - Users can tap on a product to navigate to the detailed product screen.
- **CustomTextStyles**: The app includes various custom text styles for consistent typography across the app, such as large and small body text, headlines, and titles.

### Dependencies

- `flutter_bloc`: Manages the state of the product list using the BLoC (Business Logic Component) pattern.
- `http`: Used for making network requests to the API.
- `cached_network_image`: Handles image caching for product images.
- `shared_preferences`: Used for storing cached product data locally.
- `connectivity`: For detecting network connectivity status.
